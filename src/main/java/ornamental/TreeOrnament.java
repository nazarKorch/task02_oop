package ornamental;

public class TreeOrnament extends Ornament {

  private final char c = '.';
  private final String nameOfTree;
  private final double priceOfTree;
  private final String colorOfTree;
  private final double heightOfTree;
  private String decorName = this.getClass().toString();

  public TreeOrnament(final String name, final double price,
      final String color, final double height) {
    nameOfTree = name;
    priceOfTree = price;
    colorOfTree = color;
    heightOfTree = height;

  }

  public TreeOrnament(final String name, final double price,
      final double height) {
    nameOfTree = name;
    priceOfTree = price;
    colorOfTree = "green";
    heightOfTree = height;

  }

  public TreeOrnament(final String name, final double price) {
    nameOfTree = name;
    priceOfTree = price;
    colorOfTree = "none";
    heightOfTree = 2;
  }


  public final double getPrice() {
    return priceOfTree;
  }

  public final String getName() {
    return nameOfTree;
  }

  public final String getColor() {
    return colorOfTree;
  }

  public final double getHeightOfTree() {
    return heightOfTree;
  }

  public final String getNameOfDecor() {

    int i = decorName.indexOf(c) + 1;
    decorName = decorName.substring(i, decorName.length());
    return decorName;

  }

}
