package ornamental;

public class ChristmasClothes extends Ornament {

  private final char c = '.';
  private final String nameOfOrnament;
  private final double priceOfOrnament;
  private final String colorOfOrnament;
  private final String sizeOfCloth;
  private String decorName = this.getClass().toString();

  public ChristmasClothes(final String name, final double price,
      final String color, final String size) {
    nameOfOrnament = name;
    priceOfOrnament = price;
    colorOfOrnament = color;
    sizeOfCloth = size;
  }

  public ChristmasClothes(final String name, final double price,
      final String color) {
    nameOfOrnament = name;
    priceOfOrnament = price;
    colorOfOrnament = color;
    sizeOfCloth = "fits every";
  }

  public ChristmasClothes(final String name, final int price) {
    nameOfOrnament = name;
    priceOfOrnament = price;
    colorOfOrnament = "none";
    sizeOfCloth = "Medium";
  }

  public final double getPrice() {
    return priceOfOrnament;
  }

  public final String getName() {
    return nameOfOrnament;
  }

  public final String getColor() {
    return colorOfOrnament;
  }

  public final String getSizeOfCloth() {
    return sizeOfCloth;
  }

  public final String getNameOfDecor() {

    int i = decorName.indexOf(c) + 1;
    decorName = decorName.substring(i, decorName.length());
    return decorName;

  }
}
