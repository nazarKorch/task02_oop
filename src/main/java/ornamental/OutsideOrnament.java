package ornamental;

public class OutsideOrnament extends Ornament {

  private final char c = '.';
  private final String nameOfOrnament;
  private final double priceOfOrnament;
  private final String colorOfOrnament;
  private String decorName = this.getClass().toString();

  public OutsideOrnament(final String name, final double price,
      final String color) {
    nameOfOrnament = name;
    priceOfOrnament = price;
    colorOfOrnament = color;
  }

  public OutsideOrnament(final String name, final double price) {
    nameOfOrnament = name;
    priceOfOrnament = price;
    colorOfOrnament = "none";
  }

  public final double getPrice() {
    return priceOfOrnament;
  }

  public final String getName() {
    return nameOfOrnament;
  }

  public final String getColor() {
    return colorOfOrnament;
  }

  public final String getNameOfDecor() {

    int i = decorName.indexOf(c) + 1;
    decorName = decorName.substring(i, decorName.length());
    return decorName;
  }
}
