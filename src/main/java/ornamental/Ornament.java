package ornamental;

public abstract class Ornament {


  public abstract double getPrice();

  public abstract String getName();

  public abstract String getColor();

  public abstract String getNameOfDecor();

}
