package manager;

import java.util.ArrayList;
import java.util.Scanner;
import ornamental.Ornament;

public class InputFromUser {

  private InputFromUser() {

  }

  private static int maxLength = 4;

  static void lengthDecrement() {
    maxLength--;
  }


  static int scanOfNumber(final ArrayList<Ornament> ar) {
    int nScan = 0;
    Scanner sc = new Scanner(System.in);
    try {
      nScan = sc.nextInt();
      if ((String.valueOf(nScan).length() > maxLength)
          || (String.valueOf(nScan).length() < 0)) {
        System.out.println("Please enter one from list");
        if (maxLength == 0) {
          maxLength = 4;
          OrnamentManager.refreshN();
        }
        Menu.showMainMenu(ar);
      }

    } catch (Exception e) {
      System.out.println("Exception: " + e.getMessage());
      System.out.println("Please enter one from list");
      Menu.showMainMenu(ar);
    }
    return nScan;
  }

}
