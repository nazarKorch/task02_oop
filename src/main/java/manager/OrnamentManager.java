package manager;

import java.util.ArrayList;
import ornamental.*;


public final class OrnamentManager {

  private OrnamentManager() {
  }

  private final static int DECIM = 10;
  private static int n = 0;
  private static boolean nShown = false;

  static void refreshN() {
    n = 0;
  }

  static void showOrnament(final ArrayList<Ornament> ar) {
    System.out.println(n);
    int nScanner = 0;
    int j = n;
    nShown = false;

    nScanner = InputFromUser.scanOfNumber(ar);

    if (n == 0) {
      if (nScanner == 0) {
        System.exit(0);
      }
      n = nScanner;
      InputFromUser.lengthDecrement();
    } else {
      if (nScanner == 0) {
        n = 0;
        Menu.showMainMenu(ar);
      }

      for (int i = 0; i < String.valueOf(n).length(); i++) {
        if ((j % DECIM) == nScanner) {
          System.out.println("Already showed!");
          System.out.println();
          nShown = true;
          break;
        }
        j /= DECIM;
      }

      if (!nShown) {
        n = (n * DECIM) + nScanner;
        InputFromUser.lengthDecrement();
      }
    }

    if (String.valueOf(n).length() > 1) {
      int k = n;
      int a = 0;

      for (int i = 0; i < String.valueOf(n).length(); i++) {

        a = k % DECIM;
        k /= DECIM;
        switchTheInput(ar, a);
      }

    } else {
      switchTheInput(ar, n);
    }
    Menu.showSecondMenu(ar);

  }

  static void switchTheInput(final ArrayList<Ornament> arList, final int a) {
    switch (a) {
      case 0:
        nShown = false;
        n = 0;
        Menu.showMainMenu(arList);
        break;
      case 1:
        String s = "InsideOrnament";
        for (Ornament o : arList) {
          if (o.getNameOfDecor().equals(s)) {
            System.out.println(

                "Inside decor: Ornament name: " + o.getName() + "; Price: "
                    + o.getPrice() + "$" + "; Color: " + o.getColor() + ";");
          }
        }
        break;
      case 2:
        String s2 = "OutsideOrnament";
        for (Ornament o : arList) {
          if (o.getNameOfDecor().equals(s2)) {
            System.out.println(
                "Outside decor:  Ornament name: " + o.getName() + "; Price: "
                    + o.getPrice() + "$" + "; Color: " + o.getColor() + ";");
          }
        }
        break;
      case 3:
        String s3 = "TreeOrnament";
        for (Ornament o : arList) {
          if (o.getNameOfDecor().equals(s3)) {
            System.out.println(
                "Christmas trees:  Ornament name: " + o.getName() + "; Price: "
                    + o.getPrice() + "$" + "; Color: " + o.getColor() + ";");
          }
        }
        break;
      case 4:
        String s4 = "ChristmasClothes";
        for (Ornament o : arList) {
          if (o.getNameOfDecor().equals(s4)) {
            System.out.println(
                "Christmas clothes/cloth:  Ornament name: " + o.getName()
                    + "; Price: "
                    + o.getPrice() + "$" + "; Color: " + o.getColor() + ";");
          }
        }
        break;
      default:
        System.out.println("Wrong number");
        Menu.showMainMenu(arList);
        break;
    }

  }

}
