package manager;

import java.util.ArrayList;
import ornamental.*;

public class Menu {

  private Menu() {
  }

  private static String decor1 = "1 - Inside of building/room decor";
  private static String decor2 = "2 - Outside building decor";
  private static String decor3 = "3 - Christmas trees";
  private static String decor4 = "4 - Christmas clothes, and decor";

  public static void showMainMenu(final ArrayList<Ornament> aList) {
    System.out.println("What kind of decor do you want, please enter:");
    System.out.println(decor1);
    System.out.println(decor2);
    System.out.println(decor3);
    System.out.println(decor4);
    System.out.println("0 - exit");
    OrnamentManager.showOrnament(aList);
  }

  public static void showSecondMenu(final ArrayList<Ornament> arList) {
    System.out.println();
    System.out.println("If tou want to add another kind of decor, enter: ");
    System.out.println(decor1);
    System.out.println(decor2);
    System.out.println(decor3);
    System.out.println(decor4);
    System.out.println("0 - main menu");
    OrnamentManager.showOrnament(arList);

  }

}
