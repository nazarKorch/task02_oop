package manager;

import java.util.ArrayList;
import java.util.Comparator;
import ornamental.*;

public class ListOfOrnament {

  public static ArrayList<Ornament> getList() {
    Comparator<Ornament> c = (Ornament o1, Ornament o2) -> {
      return o1.getNameOfDecor().compareTo(o2.getNameOfDecor());
    };

    ArrayList<Ornament> ar = new ArrayList<Ornament>();
    ar.add(new InsideOrnament("Christmas tree balls, 5 pieces", 10.99, "red"));
    ar.add(new OutsideOrnament("Big ringle bell", 10, "red"));
    ar.add(new TreeOrnament("Artifitial christmas tree", 299, "flocked tree",
        6));
    ar.add(new TreeOrnament("Artifitial tree with potted stand", 130.99, 6.5));
    ar.add(new ChristmasClothes("Magical christmas table cloth", 67.99,
        "green with ornament", "180x160"));
    ar.add(new TreeOrnament("Green pine tree", 220, "little flocked", 5.5));
    ar.add(new InsideOrnament("Tree topper \"Yellow Star\"", 7.59, "yellow"));
    ar.add(new InsideOrnament("Christmas angels figurine, 3 pieces", 15.50));
    ar.add(new InsideOrnament("Magical snowflakes, 5 pieces", 12.99, "white"));
    ar.add(new OutsideOrnament("Big garland with red tape on entrance door",
        150));
    ar.add(new InsideOrnament("Glitters, 1 piece", 5));
    ar.add(new InsideOrnament("Candy cane, 7 pieces", 22.50, "white and red"));
    ar.add(new ChristmasClothes("Christmas napkins, 50 pieces", 10.99,
        "green with snowflakes"));
    ar.add(new ChristmasClothes("Chistmas santa hat, 1 piece", 19.99,
        "red and white"));
    ar.add(new ChristmasClothes("Christmas socks, 7 pieces", 30.55,
        "red with stars"));
    ar.add(new OutsideOrnament("Garlands with lights", 150.25));
    ar.add(new TreeOrnament("Real big tree for outside", 350, 7.5));
    ar.add(new ChristmasClothes("Santa christmas beard", 15, "white"));
    ar.add(new ChristmasClothes("Christmas santa suit", 169, "red and white"));
    ar.add(new TreeOrnament("White artifitial tree", 339));
    ar.add(new TreeOrnament("Slim christmas tree with topper", 239));
    ar.add(new OutsideOrnament("Snow ornament on entrance door", 26.99,
        "white, green, red"));
    ar.add(new OutsideOrnament("Glass round ball on door", 11.99));
    ar.add(new OutsideOrnament("Candy cane ornament on door", 23.23,
        "red, yellow"));
    ar.add(new InsideOrnament("Balls on tree, 7 pieces", 33,
        "yellow, white"));
    ar.add(new InsideOrnament("Glitter glass balls, 16 pieces", 67.99, "green"));
    return ar;
  }
}
